import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import Layout from './Layout';
import { getAllMessages } from './redux/actions/messages';

const App = ()=> {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllMessages());
    }, [dispatch])

    return (
        <Layout/>
    );
}

export default App;
