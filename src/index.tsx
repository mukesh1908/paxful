import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Provider, useDispatch } from 'react-redux';

import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";

import store from './redux/store';

import './css/index.scss';
import App from './App';
import UserDetail from './container/UserDetail';
import NotFound from './container/NotFound';
import { getExchangeRate } from './redux/actions/exchangeRate';

const ScrollToTop = ():null => {
  window.scrollTo(0, 0);
  return null;
};

const MainRoutes = ()=> {
    const dispatch = useDispatch();

    useEffect(()=> {
        dispatch(getExchangeRate());
        const interval = setInterval(() => {
            dispatch(getExchangeRate());
        }, 5000);
        return () => clearInterval(interval);
    }, [dispatch])

    return(
        <Switch>
            <Route exact path="/" component={App}/>
            <Route exact path="/trade/:tradeId" component={App}/>
            <Route exact path="/tradeDetail/:tradeId" component={UserDetail}/>
            <Route path="/*" component={NotFound}/>
        </Switch>
    )
}
ReactDOM.render(
  <Provider store={store}>
    <Router>
        <Route component={ScrollToTop} />
        <MainRoutes />
    </Router>
  </Provider>,
  document.getElementById('root')
);
