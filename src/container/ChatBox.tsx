import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { IMessage } from '../types/message';
import NewMessage from '../components/ChatBox/NewMessage';
import ChatHeader from '../components/ChatBox/ChatHeader';
import ChatList from '../components/ChatBox/ChatList';
import Placeholder from '../components/Placeholder/Placeholder';

import { getAllUserMessages } from '../redux/actions/userMessages';

import './style/chatbox.scss';

const ChatBox = ()=>{
    const { tradeId } = useParams();
    const dispatch = useDispatch();

    const messages = useSelector((state:any) => {
        return state.messages
    });
    const selectedMessage = messages.find((msg:IMessage)=>msg.id===parseInt(tradeId))

    // fetch conversation for this trade.
    useEffect(()=>{
        dispatch(getAllUserMessages(parseInt(tradeId)))
    }, [tradeId, dispatch])

    const userMessages = useSelector((state:any) => {
        return state.userMessages
    });

    return(
        <>
        {
            tradeId ?
            <>
                <ChatHeader {...selectedMessage}/>
                <div className="chatbox--container">
                    {
                        userMessages && userMessages.length ?
                        <ChatList data={userMessages}/>
                        :
                        <div>
                            <Placeholder text="No message available." />
                        </div>
                    }
                </div>
                <NewMessage/>
            </>
            :
            <div>
                <Placeholder text="Please select a trade to start chatting." />
            </div>
        }
        </>
    )
}

export default ChatBox;
