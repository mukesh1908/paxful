import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getTradeDetail } from '../redux/actions/tradeDetail';
import TradeDetail from '../components/TradeDetail';

const UserDetail = ()=> {
    const { tradeId } = useParams();
    const dispatch = useDispatch();

    const tradeDetail = useSelector((state:any) => {
        return state.tradeDetail
    });

    useEffect(()=> {
        tradeId && dispatch(getTradeDetail(parseInt(tradeId)))
    }, [ tradeId, dispatch ])

    if(!tradeDetail || !tradeId) {
        return null
    }

    return(
        <TradeDetail {...tradeDetail}/>
    )
}

export default UserDetail;
