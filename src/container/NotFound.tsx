import React from 'react';
import { Link } from 'react-router-dom';
import Placeholder from '../components/Placeholder/Placeholder';

import './style/notfound.scss';

const NotFound = ()=> (
    <div className="notfound--container">
        <Placeholder text="Sorry, page not found" />
        <Link to="/">Return to Home</Link>
    </div>
)

export default NotFound;
