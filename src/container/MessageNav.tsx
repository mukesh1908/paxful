import React from 'react';
import MessageCard from '../components/MessageCard';
import { IMessage } from '../types/message';

import { useSelector } from 'react-redux';

const MessageNav = ()=> {
    const messages = useSelector((state:any) => {
        return state.messages
    });

    return messages.map((msg:IMessage)=><MessageCard {...msg} key={msg.id}/>)
}

export default MessageNav;
