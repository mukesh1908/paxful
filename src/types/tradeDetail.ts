export interface ITradeDetail {
    "tradeId": number,
    "noOfTrade": number,
    "tradeStatus": string,
    "tradeHash": string,
    "amount": number,
    "username": string,
    "avatar": string
}
