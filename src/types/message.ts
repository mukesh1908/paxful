export interface IMessage {
    id: number,
    username: string,
    avatar: string,
    tradeName: string,
    paymentMethod: string,
    status: string,
    amount: number,
    isNew: boolean
}
