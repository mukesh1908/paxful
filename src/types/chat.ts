export interface IChat {
    owner: boolean,
    avatar: string,
    body: string
}
