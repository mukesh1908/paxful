import React from 'react';
import { useHistory } from 'react-router-dom';
import { ITradeDetail } from '../../types/tradeDetail';

import { useSelector } from 'react-redux';

import './style.scss';
import defaultAvatar from '../../assets/people.svg';

const TradeDetail = ({
        username,
        noOfTrade,
        tradeStatus,
        tradeHash,
        amount,
        avatar
    }: ITradeDetail )=> {

        const history = useHistory();
        const backHandler = ()=> {
            history.goBack();
        }

        const exchangeRate = useSelector((state:any) => {
            return state.exchangeRate
        });

        return (
            <div className="trade-detail-container">
                <div className="user-detail--title">
                    You are trading with <span className="bold">{username}</span>
                </div>
                <div className="user-detail--sub-title">
                    Started 23 minutes ago
                </div>
                <div className="user-detail-btn-container">
                    <div className="user-detail-btn">Release bitcoins</div>
                </div>
                <div className="user-detail-meta">
                    <div className="border-bottom border-right">
                        <div className="avatar">
                            <img src={avatar || defaultAvatar} alt={username}/>
                        </div>
                    </div>
                    <div className="border-bottom">
                        <div className="user-detail-meta--title bold"># of trade</div>
                        <div className="bolder">{noOfTrade}</div>
                    </div>
                    <div className="border-bottom border-right">
                        <div className="user-detail-meta--title bold">Trade status</div>
                        <div className="bolder">{tradeStatus}</div>
                    </div>
                    <div className="border-bottom">
                        <div className="user-detail-meta--title bold">Trade hash</div>
                        <div className="bolder">{tradeHash}</div>
                    </div>
                    <div className="border-right">
                        <div className="user-detail-meta--title bold">Amount USD</div>
                        <div className="bolder">{amount} USD</div>
                    </div>
                    <div>
                        <div className="user-detail-meta--title bold">Amount BTC</div>
                        <div className="bolder">
                            {exchangeRate ? (amount/exchangeRate).toFixed(8) : "--"}
                        </div>
                    </div>
                </div>
                <div className="back-to-chat--mobile">
                    <div className="back-to-chat-btn" onClick={backHandler}>Back</div>
                </div>
            </div>
        )
}

export default TradeDetail
