import React from 'react';
import './placeholder.scss';

const Placeholder = ({text}:{text:string})=>(
    <div className="placeholder-container--default">{text}</div>
)

export default Placeholder;
