import React, { useState, FC } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IMessage } from '../../types/message';
import avatarImage from '../../assets/people.svg';

import './style.scss';

const statusToStrMap:{ [ key: string ]: string } = {
    PAID: 'Paid',
    NOT_PAID: 'Not paid'
}

const MessageCard:FC<IMessage> = ({ id, username, tradeName, amount, status, isNew, avatar}) => {
    const history = useHistory();
    const { tradeId } = useParams();

    const [ seen, setSeen ] = useState(isNew);

    const clickHandler = ()=> {
        setSeen(false);
        history.push(`/trade/${id}`)
    }

    const exchangeRate = useSelector((state:any) => {
        return state.exchangeRate
    });

    return(
        <div className={(parseInt(tradeId) === id ? "selected" : "") +" new-message-card"} onClick={clickHandler as any}>
            <div className="new-message--marker">
                {
                    seen && (<div className="new-marker"></div>)
                }
            </div>
            <div className="new-message--body">
                <div className="note bold username">{username} is buying</div>
                <div className="bold tradename ">{tradeName}</div>
                <div className="note">{amount} USD ({ exchangeRate ? (amount/exchangeRate).toFixed(8) : '--'} BTC)</div>
            </div>
            <div className="new-message--meta">
                <div className="avatar">
                    <img alt={username} src={avatar || avatarImage} />
                </div>
                <div className={status.toLowerCase()+ " status"}>{statusToStrMap[status]}</div>
            </div>
        </div>
    )
}

export default MessageCard;
