import React, { useEffect, useRef } from 'react';
import { IChat } from '../../types/chat';
import avatar from '../../assets/people.svg';

import './style/chat-list.scss';
const ChatList = ({ data }:{ data:IChat[] }) => {

    const containerRef = useRef<HTMLHeadingElement>();


    useEffect(()=>{
        containerRef.current.scrollIntoView({
          behavior: 'smooth',
          block: 'start'
        });
    }, [ data ])

    return(
        <div className="chat-list-section">
            {
                data.map((msg, i)=> {
                    return (
                        <div  ref={containerRef} key={i} className={(msg.owner ? "owner": "")+" chat-row"}>
                            <div className="chat-avatar">
                                <img src={ msg.avatar || avatar } alt="avatar-icon"/>
                            </div>
                            <div className="chat-row-content">
                                { msg.body }
                                <div className="chat-arrow"></div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}


export default ChatList
