import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import deleteIcon from '../../assets/delete.svg';
import nextIcon from '../../assets/next.svg';
import { deleteMessage } from '../../redux/actions/messages';

import './style/chat-header.scss';

const ChatHeader = (props:any) => {
    const { username, tradeName } = props;
    const dispatch = useDispatch();
    const history = useHistory();
    const { tradeId } = useParams();

    const clickHandler = ()=> {
        dispatch(deleteMessage(parseInt(tradeId)))
        history.push('/')
    }

    const navigateToTradeDetail = ()=> {
        history.push(`/tradeDetail/${tradeId}`)
    }

    return(
        <div className="chat-header-section">
            <div className="delete-message" onClick={clickHandler}>
                <img src={deleteIcon} alt="delete-message"/>
            </div>
            <div>
                <div className="bold chat-header-title">{tradeName}</div>
                <div  className="note chat-header-subtitle">{username}</div>
            </div>
            <div className="trade-detail--mobile" onClick={navigateToTradeDetail}>
                <img src={nextIcon} alt="delete-message"/>
            </div>
        </div>
    )
}


export default ChatHeader
