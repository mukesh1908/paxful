import React, { useState, SyntheticEvent } from 'react';
import { useDispatch } from 'react-redux';
import { sendUserMessages } from '../../redux/actions/userMessages';

import './style/new-message.scss';

const NewMessageTextBox = () => {
    const [ message, setMessage ] = useState('');
    const dispatch = useDispatch();

    const sendMessage = ()=> {
        if(message) {
            dispatch(sendUserMessages(message));
            setMessage('')
        }
    }

    const handleSubmit = (e:SyntheticEvent)=> {
        e.preventDefault();
        dispatch(sendUserMessages(message));
        setMessage('')
    }
    return(
        <div className="new-message">
            <div className="content">
                <div className="textbox">
                    <form onSubmit={handleSubmit}>
                        <input required type="text" placeholder="Type your message" value={message} onChange={(e)=>setMessage(e.target.value)} />
                    </form>
                </div>
                <div className="new-message-button bold" onClick={sendMessage as any}>
                    Send
                </div>
            </div>
        </div>
    )
}


export default NewMessageTextBox
