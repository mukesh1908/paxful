import { IAction } from '../actions/messages';
import { IMessage } from '../../types/message';

export const messages = (state:IMessage[]=[], action:IAction)=>{
	switch (action.type) {
		case "SET_ALL_MESSAGES":
			return action.payload
		case "DELETE_MESSAGE":
			const updatedList = state.filter((msg)=>msg.id!==action.payload);
			return updatedList
		default:
			return state

	}
}
