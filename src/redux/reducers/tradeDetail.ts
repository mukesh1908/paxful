import { IAction } from '../actions/tradeDetail';
import { ITradeDetail } from '../../types/tradeDetail';

export const tradeDetail = (state:ITradeDetail=null, action:IAction)=>{
	switch (action.type) {
		case "SET_TRADE_DETAIL":
			return action.payload
		default:
			return state

	}
}
