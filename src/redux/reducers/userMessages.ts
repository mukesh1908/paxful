import { IAction } from '../actions/userMessages';
import { IChat } from '../../types/chat';

export const userMessages = (state:IChat[]=[], action:IAction)=>{
	switch (action.type) {
		case "SET_ALL_USER_MESSAGES":
			return action.payload
		case "ADD_NEW_USER_MESSAGE":			
			return state.concat(action.payload)
		default:
			return state

	}
}
