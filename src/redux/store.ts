import { applyMiddleware, createStore, combineReducers} from 'redux';
import {
    messages,
    userMessages,
    tradeDetail,
    exchangeRate
} from './reducers';

import thunk from 'redux-thunk';

const reducer = combineReducers({
    messages,
    userMessages,
    tradeDetail,
    exchangeRate
})

const middleWare = applyMiddleware(thunk)
const store = createStore(reducer, middleWare);

declare global {
    interface Window { _store: any; }
}

window._store = store

export default store;
