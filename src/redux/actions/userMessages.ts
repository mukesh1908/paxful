import mockUserMessages from './mock/mockUserMessages.json';
import { ThunkDispatch } from 'redux-thunk';
import { IChat } from '../../types/chat';

export interface IAction {
    type: string,
    payload: IChat[]
}

export interface IAddNewAction {
    type: string,
    payload: IChat
}


const setAllUserMessages = (messages:IChat[]):IAction => ({
    type: "SET_ALL_USER_MESSAGES",
    payload: messages
})

const addNewUserMessage = (message:IChat):IAddNewAction => ({
    type: "ADD_NEW_USER_MESSAGE",
    payload: message
})



export const getAllUserMessages = (tradeId:number) => {
    // in real life, it will make an API call and get real data from server
    const userMessages = mockUserMessages.find((msg)=> msg.tradeId === tradeId);
	return function(dispatch:ThunkDispatch<{}, {}, any>){
		dispatch(setAllUserMessages(((userMessages && userMessages.messages) || [])))
	}
}

export const sendUserMessages = (message:string) => {
    // in real life, it will make an API call to post message
	return function(dispatch:ThunkDispatch<{}, {}, any>){
		dispatch(addNewUserMessage({
            avatar: "",
            owner: true,
            body: message
        }))
	}
}
