import mockTradeDetails from './mock/mockTradeDetail.json';
import { ThunkDispatch } from 'redux-thunk';
import { ITradeDetail } from '../../types/tradeDetail';

export interface IAction {
    type: string,
    payload: ITradeDetail
}

const setTradeDetail = (messages:ITradeDetail):IAction => ({
    type: "SET_TRADE_DETAIL",
    payload: messages
})


export const getTradeDetail = (tradeId:number) => {
    // in real life, it will make an API call and get real data from server
    const tradeDetail = mockTradeDetails.find((trade)=>trade.tradeId === tradeId)
	return function(dispatch:ThunkDispatch<{}, {}, any>){
		dispatch(setTradeDetail(tradeDetail))
	}
}
