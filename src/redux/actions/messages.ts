import mockMessages from './mock/mockMessages.json';
import { ThunkDispatch } from 'redux-thunk';
import { IMessage } from '../../types/message';

export interface IAction {
    type: string,
    payload: IMessage[] | number
}

const setAllMessages = (messages:IMessage[]):IAction => ({
    type: "SET_ALL_MESSAGES",
    payload: messages
})

const deleteMessageById = (tradeId:number):IAction => ({
    type: "DELETE_MESSAGE",
    payload: tradeId
})

export const getAllMessages = () => {
    // in real life, it will make an API call and get real data from server
	return function(dispatch:ThunkDispatch<{}, {}, any>){
		dispatch(setAllMessages(mockMessages))
	}
}

export const deleteMessage = (tradeId: number) => {
	return function(dispatch:ThunkDispatch<{}, {}, any>){
		dispatch(deleteMessageById(tradeId))
	}
}
