import { ThunkDispatch } from 'redux-thunk';
import fetchExchangeRate from '../../api/exchangeRate';

const setExchangeRate = (rate:number):any => ({
    type: "SET_EXCHANGE_RATE",
    payload: rate
})


export const getExchangeRate = () => {
    return async function(dispatch:ThunkDispatch<{}, {}, any>){
        const rate = await fetchExchangeRate();
		dispatch(setExchangeRate(rate.bpi.USD.rate_float))
	}
}
