import React from 'react';
import { useParams, Link } from 'react-router-dom';
import './layout.scss';
import MessageNav from '../container/MessageNav';
import ChatBox from '../container/ChatBox';
import UserDetail from '../container/UserDetail';
import logo from '../assets/logo.png';

const Layout = ()=> {
    const { tradeId } = useParams();
    return(
        <div className={(tradeId ? "trade-detail-active" :"")+" layout--main"}>
            <div className="header">
                <div className="subheader-1">
                    <Link to="/">
                        <img src={logo} alt="paxful" />
                    </Link>
                </div>
                <div className="subheader-2">
                    <Link to="/" className="tabs selected"><div>Trades</div></Link>
                    <Link to="/tab1" className="tabs"><div>Tab2</div></Link>
                    <Link to="/tab2" className="tabs"><div>Tab3</div></Link>
                </div>
            </div>
            <div className="container--main">
                <div className="left">
                    <MessageNav />
                </div>
                <div className="center">
                    <ChatBox/>
                </div>
                <div className="right">
                    <UserDetail />
                </div>
            </div>
        </div>
    )
}
export default Layout
