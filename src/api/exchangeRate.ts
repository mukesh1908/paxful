const fetchExchangeRate = () =>
  fetch('https://api.coindesk.com/v1/bpi/currentprice/USD.json').then(res => res.json());

export default fetchExchangeRate;
